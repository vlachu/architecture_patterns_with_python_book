# Используйте официальный образ Python как базовый образ
FROM python:3.10

# Установите Poetry
RUN pip install poetry

# Установите рабочую директорию в контейнере
WORKDIR /app

# Копируйте файлы pyproject.toml и (опционально) poetry.lock в рабочую директорию
COPY pyproject.toml poetry.lock* /app/

# Отключите создание виртуальной среды Poetry и установите зависимости
# Если файл poetry.lock отсутствует, звездочка (*) позволит команде COPY успешно выполниться
RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi --no-root

# Копируйте исходный код проекта в рабочую директорию
COPY . /app

# Укажите команду для запуска приложения
CMD ["python", "architecture_patterns_with_python_book.py"]